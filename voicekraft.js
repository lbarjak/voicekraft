"use strict";
//lbarjak 2018.05.02.
var fs = require("fs");
const downloads = require("./login.js").downloads;

var hangzavar = require("./modules/hangzavar"); //JSON file
var cikksz = Object.assign(
    hangzavar["AV-LEADER"],
    hangzavar["Elder Audio"],
    hangzavar["FS Audio"],
    hangzavar["Phonic"],
    hangzavar["Roxtone"],
    hangzavar["Seetronic"],
    hangzavar["Voice-Kraft"]);
var cikkszam = [];
Object.keys(cikksz).forEach(function (key, index) {
    cikkszam.push(key);
});
var status1 = {}, //van-nincs állapot
    netto_arak = {};
var anyagkod = 0, //-->cikkszám
    allapot = "", //van-nincs
    netto_ar = "";
var sor0 = "",
    sor1 = "",
    sor2 = "";

function processHTML(inputFile) {
    var fs = require("fs"),
        readline = require("readline"),
        instream = fs.createReadStream(inputFile),
        outstream = new(require("stream"))(),
        rl = readline.createInterface(instream, outstream);

    rl.on("line", function (line) {
        sor0 = sor1;
        sor1 = sor2;
        sor2 = line;
        if (/(?<="anyagkod">)\d+/.test(sor0)) {
            anyagkod = sor0.match(/(?<="anyagkod">)\d+/)[0];
            if (/("van"|"nincs")>(<b>)?(van|nincs)/.test(sor1)) {
                allapot = sor1.match(/(?<=("van"|"nincs")>(<b>)?)(van|nincs)/)[0];
            } else {
                allapot = undefined;
            }
            status1[anyagkod] = allapot;
            //if (/(?<="ar">)\d+ ?\d+/.test(sor2)) {
            //    netto_ar = sor2.match(/(?<="ar">)\d+ ?\d+/)[0].replace(" ", "");
            if (/(?<="ar">)(<b>)?\d+ ?\d+/.test(sor2)) {
                 netto_ar = sor2.match(/(?<="ar">)(<b>)?\d+ ?\d+/)[0].replace(" ", "").replace("<b>", "");
            } else {
                netto_ar = undefined;
            }
            netto_arak[anyagkod] = netto_ar;
        }
    });
    rl.on("close", function (line) {
        change()
    });
}

downloads(); //TODO callback?

var timer = setTimeout(function () {
    processHTML("./modules/arlista_hu.html");
}, 1000);

function change() {
    var status2 = []; //1000, 0, "-"
    var netto = [];
    var to_send = [];
    var nincs = ["Ezek nincsenek:"];
    for (var i = 0; i < cikkszam.length; i++) {
        //for (var i = 0; i < 5; i++) {
        var sor = [];
        if (status1[cikkszam[i]] == "van") status2.push("1000");
        if (status1[cikkszam[i]] == "nincs") status2.push("0");
        if (status1[cikkszam[i]] == undefined) status2.push("-");
        //console.log(i, cikkszam[i], status2[i], netto_arak[cikkszam[i]], cikksz[cikkszam[i]]);
        //to API:
        //console.log(cikksz[cikkszam[i]], status2[i], netto_arak[cikkszam[i]]);
        sor.push(cikksz[cikkszam[i]]);
        sor.push(status2[i]);
        sor.push(netto_arak[cikkszam[i]]);
        sor.push(cikkszam[i]);
        if (status2[i] == "-") {
            nincs.push(cikkszam[i]);
        } else {
            to_send.push(sor);
        }
        //to csv:
        //console.log(cikkszam[i]+";"+status2[i]+";"+netto_arak[cikkszam[i]]);
    }
    var nincs_json = JSON.stringify(nincs);
    fs.writeFile("./modules/nincs.json", nincs_json, function (err) {
        if (err) {
            return console.log(err);
        }
    });
    var to_send_json = JSON.stringify(to_send);
    fs.writeFile("./modules/to_send.json", to_send_json, function (err) {
        if (err) {
            return console.log(err);
        }
    });
}